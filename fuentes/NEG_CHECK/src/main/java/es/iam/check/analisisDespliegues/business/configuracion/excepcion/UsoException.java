package es.iam.check.analisisDespliegues.business.configuracion.excepcion;

/**
 * Informa de un error en el uso de la aplicacion.
 * @author SSA005
 */
public class UsoException extends Exception {

	/**
	 * id para la serializacion.
	 */
	private static final long serialVersionUID = 1568989055934701158L;

}
