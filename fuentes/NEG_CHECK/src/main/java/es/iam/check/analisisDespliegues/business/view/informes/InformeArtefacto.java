package es.iam.check.analisisDespliegues.business.view.informes;

/**
 * Clase que indica los nombres de los informes de artefactos.
 * @author SSA005
 * @author ARD021
 */

public class InformeArtefacto extends AbstractHtmlInforme{
	
	/**
	 * Nombre de lo informes de artefactos.
	 */
	public final static String NOMBRE = "InformeArtefacto.html";
	
	/**
	 * Constructor por defecto.
	 */
	public InformeArtefacto(){
		setNombre(NOMBRE);
	}
	
}
