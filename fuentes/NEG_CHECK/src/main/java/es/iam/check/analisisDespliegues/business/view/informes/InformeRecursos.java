package es.iam.check.analisisDespliegues.business.view.informes;


/**
 * Clase que indica los nombres de los informes de recursos.
 * @author SSA005
 * @author ARD021
 */

public class InformeRecursos extends AbstractHtmlInforme{
	
	/**
	 * Nombre de lo informes de recursos.
	 */
	public final static String NOMBRE = "InformeRecursos.html";

	/**
	 * Constructor por defecto.
	 */
	public InformeRecursos(){
		setNombre(NOMBRE);
	}
}
