package es.iam.check.analisisDespliegues.business.configuracion;

/**
 * Tipos de servidor sobre el que se pueden ejecutar los chequeos.
 * @author ARD021
 */
public enum TipoServidor {
	
	WAS61,
	WAS85,
	JBOSS
	

}
