package es.iam.check.analisisDespliegues.business.chequeos.reglas;

/**
 * Tipos de comprobacion que se pueden hacer sobre un archivo.
 * @author SSA005
 */
public enum TipoComprobacion {
	
	EXISTENCIA,
	OBLIGATORIEDAD,
	PROHIBIDO;

}
